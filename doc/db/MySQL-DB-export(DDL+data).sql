-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 15, 2019 at 01:16 AM
-- Server version: 5.7.25
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `XXXYYYZZZ`
--

-- --------------------------------------------------------

--
-- Table structure for table `attend`
--

CREATE TABLE `attend` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `meeting_id` int(11) NOT NULL,
  `role` enum('organiser','attende','alien') DEFAULT NULL,
  `going` enum('yes','no','maybe','location_change_request','time_change_request') DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attend`
--

INSERT INTO `attend` (`id`, `user_id`, `meeting_id`, `role`, `going`) VALUES(1, 2, 3, 'organiser', 'yes');
INSERT INTO `attend` (`id`, `user_id`, `meeting_id`, `role`, `going`) VALUES(2, 1, 2, 'attende', 'maybe');
INSERT INTO `attend` (`id`, `user_id`, `meeting_id`, `role`, `going`) VALUES(3, 1, 1, 'organiser', 'yes');
INSERT INTO `attend` (`id`, `user_id`, `meeting_id`, `role`, `going`) VALUES(4, 2, 1, 'attende', 'location_change_request');

-- --------------------------------------------------------

--
-- Table structure for table `conclusion`
--

CREATE TABLE `conclusion` (
  `id` int(11) NOT NULL,
  `meeting_id` int(11) DEFAULT NULL,
  `type` enum('next_meeting','contract','bill') DEFAULT NULL,
  `detail` text,
  `attchment` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `meeting`
--

CREATE TABLE `meeting` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `location` text,
  `start_dtime` datetime DEFAULT NULL,
  `end_dtime` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meeting`
--

INSERT INTO `meeting` (`id`, `description`, `location`, `start_dtime`, `end_dtime`) VALUES(1, 'Long term contract discussion', 'Zmajski most, Ljubljana, SI', '2020-05-01 12:00:01', '2020-05-01 13:30:00');
INSERT INTO `meeting` (`id`, `description`, `location`, `start_dtime`, `end_dtime`) VALUES(2, 'Beer', 'Munich, DE', '2021-06-10 10:30:00', '2021-06-10 13:00:00');
INSERT INTO `meeting` (`id`, `description`, `location`, `start_dtime`, `end_dtime`) VALUES(3, 'Holidays', 'Zimbabwe', '2021-06-01 10:30:00', '2021-06-01 12:00:00');
INSERT INTO `meeting` (`id`, `description`, `location`, `start_dtime`, `end_dtime`) VALUES(0, 'Skype meeting discussion', '[skype: url..y]', '2022-01-10 14:30:11', '2022-01-10 12:00:00');
INSERT INTO `meeting` (`id`, `description`, `location`, `start_dtime`, `end_dtime`) VALUES(7, 'ESA', 'Mars', '2030-01-01 14:00:00', '2030-01-01 15:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `type` enum('admin','employee','customer','alien') DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `full_name`, `email`, `password`, `phone`, `type`) VALUES(1, 'Janez Zadrti', 'janez@zadrti.com', 'janezTopSecurity', '090 12345', 'admin');
INSERT INTO `user` (`id`, `full_name`, `email`, `password`, `phone`, `type`) VALUES(2, 'Mirko Novak', 'mirko@novak.com', 'mirkoTopSecurity', '090 4321', 'employee');
INSERT INTO `user` (`id`, `full_name`, `email`, `password`, `phone`, `type`) VALUES(3, 'Francka - Lunar Manager, ESA', 'info@esa.com', 'esaTopSecurity', '-', 'customer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attend`
--
ALTER TABLE `attend`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meeting_id` (`meeting_id`);

--
-- Indexes for table `conclusion`
--
ALTER TABLE `conclusion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meeting_id` (`meeting_id`);

--
-- Indexes for table `meeting`
--
ALTER TABLE `meeting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attend`
--
ALTER TABLE `attend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `conclusion`
--
ALTER TABLE `conclusion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `meeting`
--
ALTER TABLE `meeting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
