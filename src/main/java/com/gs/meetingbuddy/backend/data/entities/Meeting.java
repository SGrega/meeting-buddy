package com.gs.meetingbuddy.backend.data.entities;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "meeting")
public class Meeting extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	// @Version
	// int version=1;

	@Id
	@GeneratedValue // (strategy = GenerationType.IDENTITY)

	private Long id = (long) -1;

	@NotBlank(message = "Description is required")
	@Size(max = 255)
	private String description;

	private String location;

	@NotNull(message = "Start time is required")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDtime;

	@NotNull(message = "End time is required")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDtime;

	// Linked with Attend entity (DB table)
	@OneToMany(mappedBy = "meetingObject", fetch = FetchType.EAGER) // cascade = CascadeType.ALL)
	private Set<Attend> attendees;

	
	// Getters AND Setters
	
	public Meeting() {
	}

	public Meeting(String description, String location, Date startDtime, Date endDtime) {

		this.description = description;
		this.location = location;
		this.startDtime = startDtime;
		this.endDtime = endDtime;
	}

	public Meeting(String description, String location) {
		this.description = description;
		this.location = location;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getStartDtime() {
		return startDtime;
	}

	public void setStartDtime(Date startDtime) {
		this.startDtime = startDtime;
	}

	public Date getEndDtime() {
		return endDtime;
	}

	public void setEndDtime(Date endDtime) {
		this.endDtime = endDtime;
	}

	public Set<Attend> getAttendees() {
		return attendees;
	}

	public void setAttendees(Set<Attend> attendees) {
		this.attendees = attendees;
	}

	public String getAttendeesString() {

		if (this.getAttendees().size() != 0) {
			String msg = "";
			for (Attend attObj : this.getAttendees()) {
				msg += "[(" + attObj.getRole() + ") " + attObj.getUserObject().getFullName() + " :" + attObj.getGoing()
						+ "] ";
			}
			return msg;
		} else {
			return "";
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		Meeting that = (Meeting) o;
		return Objects.equals(description, that.description) && Objects.equals(location, that.location);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), description, location);
	}

}
