package com.gs.meetingbuddy.backend.data.entities;

import javax.persistence.*;
import java.io.Serializable;
import com.gs.meetingbuddy.backend.data.*;

@Entity
public class Attend implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id //general ID variable to simplify instead of combining userId and meetingId as EmbeddedID
	private Long id;

	@Column(name = "user_id")
	private Long userId;

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false) // , nullable =  false)
	private User userObject;

	@ManyToOne
	@JoinColumn(name = "meeting_id", referencedColumnName = "id", insertable = false, updatable = false) // , nullable =  false)
	private Meeting meetingObject;

	@Column(name = "meeting_id")
	private Long meetingId;

	@Enumerated(EnumType.STRING)
	private RoleENUM role;

	@Enumerated(EnumType.STRING)
	private GoingENUM going;

	
	// Getters AND Setters

	public void Attend() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getMeetingId() {
		return meetingId;
	}

	public void setMeetingId(Long meetingId) {
		this.meetingId = meetingId;
	}

	public RoleENUM getRole() {
		return role;
	}

	public void setRole(RoleENUM role) {
		this.role = role;
	}

	public GoingENUM getGoing() {
		return going;
	}

	public void setGoing(GoingENUM going) {
		this.going = going;
	}

	public Meeting getMeetingObject() {
		return meetingObject;
	}

	public void setMeetingObject(Meeting meetingObject) {
		this.meetingObject = meetingObject;
	}

	public User getUserObject() {
		return userObject;
	}

	public void setUserObject(User userObject) {
		this.userObject = userObject;
	}

}
