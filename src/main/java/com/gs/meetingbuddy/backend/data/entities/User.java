package com.gs.meetingbuddy.backend.data.entities;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import com.gs.meetingbuddy.backend.data.*;

@Entity
public class User extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	// @Version
	// int version=1;

	@Id
	@GeneratedValue
	private Long id = (long) -1;

	@Size(min = 3, max = 255, message = "name must be longer than 3 and less than 255 characters")
	private String fullName;

	@Email // @Pattern(regexp = ".+@.+\\.[a-z]+", message = "Must be valid email")
	@Size(max = 255)
	private String email;

	@Size(min = 8, max = 255, message = "name must be longer than 8 and less than 255 characters")
	private String password;

	@Size(max = 255)
	private String phone;

	@Enumerated(EnumType.STRING)
	private UserTypeENUM type;

	@OneToMany(mappedBy = "userObject", fetch = FetchType.EAGER) // cascade = CascadeType.ALL)
	private Set<Attend> meetingAttendees; 

	// Getters AND Setters
	
	
	public User() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public UserTypeENUM getType() {
		return type;
	}

	public void setType(UserTypeENUM type) {
		this.type = type;
	}

	public Set<Attend> getMeetingAttendees() {
		return meetingAttendees;
	}

	public void setMeetingAttendees(Set<Attend> meetingAttendees) {
		this.meetingAttendees = meetingAttendees;
	}

	 
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		User that = (User) o;
		return Objects.equals(email, that.email) && Objects.equals(fullName, that.fullName)
				&& Objects.equals(type, that.type) && Objects.equals(phone, that.phone);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), email, fullName, type, phone);
	}

}
