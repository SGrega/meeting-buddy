package com.gs.meetingbuddy.backend.data.entities;

import java.io.Serializable;
import javax.persistence.MappedSuperclass;

/*
 * Optional abstraction class for other Entity classes
 */

@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

	/*	@Id
	@GeneratedValue
	private Long id;

	@Version
	private int version;

	public Long getId() {
		return id;
	}

	public int getVersion() {
		return version;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, version);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		AbstractEntity that = (AbstractEntity) o;
		//	return version == that.version && 				Objects.equals(id, that.id);
		return Objects.equals(id, that.id);
		
	}
	*/
}