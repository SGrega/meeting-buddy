package com.gs.meetingbuddy.backend.services;

import com.gs.meetingbuddy.backend.data.entities.AbstractEntity;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * This class provides an template to implement other similar services 
 * */
public interface FilterableCrudService<T extends AbstractEntity> extends CrudService<T> {

	Page<T> findAnyMatching(Optional<String> filter, Pageable pageable);

	long countAnyMatching(Optional<String> filter);

}