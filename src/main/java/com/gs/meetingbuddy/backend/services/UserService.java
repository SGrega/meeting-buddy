package com.gs.meetingbuddy.backend.services;

import com.gs.meetingbuddy.backend.data.entities.User;
import com.gs.meetingbuddy.backend.repositories.UserRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService implements FilterableCrudService<User> {

	public static final String MODIFY_LOCKED_USER_NOT_PERMITTED = "User has been locked and cannot be modified or deleted";
	private static final String DELETING_SELF_NOT_PERMITTED = "You cannot delete your own account";
	private final UserRepository userRepository;

	@Autowired
	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public User findById( long id) {
		return this.userRepository.findById(id).get();
	}
	
	//Data provider calls this function if UI search filter is activated -  from UI it is called as  dataProvider.setFilter(event.getValue()) ; 
	//Must match with a function countAnyMatching(...)  !
	public Page<User> findAnyMatching(Optional<String> filter, Pageable pageable) {  
		if (filter.isPresent()) {
			String repositoryFilter = "%" + filter.get() + "%";
			return getRepository()
					.findByEmailLikeIgnoreCaseOrFullNameLikeIgnoreCaseOrPhoneLikeIgnoreCase(
							repositoryFilter,  repositoryFilter, repositoryFilter, pageable);
					
		} else {
			return find(pageable);
		}
	}

	//@Override 
	//If UI search  filter activated. 
	//Must match with a function findAnyMatching(...)  !
	public long countAnyMatching(Optional<String> filter) {
		if (filter.isPresent()) {
			String repositoryFilter = "%" + filter.get() + "%";
			return userRepository.countByEmailLikeIgnoreCaseOrFullNameLikeIgnoreCaseOrPhoneLikeIgnoreCase(
					repositoryFilter, repositoryFilter, repositoryFilter);
		} else {
			return count();
		}
	}

	@Override
	public UserRepository getRepository() {
		return userRepository;
	}

	public Page<User> find(Pageable pageable) {
		return getRepository().findBy(pageable);
	}

	@Override
	public User save(User currentUser, User entity) {
		//throwIfUserLocked(entity);
		return getRepository().saveAndFlush(entity);
	}

	@Override
	@Transactional
	public void delete(User currentUser, User userToDelete) {
		//throwIfDeletingSelf(currentUser, userToDelete);
		//throwIfUserLocked(userToDelete);
		FilterableCrudService.super.delete(currentUser, userToDelete);
	}

	/*	
	private void throwIfDeletingSelf(User currentUser, User user) {
		if (currentUser.equals(user)) {
			throw new UserFriendlyDataException(DELETING_SELF_NOT_PERMITTED);
		}
	}
	
	private void throwIfUserLocked(User entity) {
		if (entity != null && entity.isLocked()) {
			throw new UserFriendlyDataException(MODIFY_LOCKED_USER_NOT_PERMITTED);
		}
	}
 	*/
	
	@Override
	public User createNew(User currentUser) {
		return new User();
	}

}