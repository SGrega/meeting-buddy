package com.gs.meetingbuddy.backend.services;

import com.gs.meetingbuddy.backend.data.entities.Meeting;
import com.gs.meetingbuddy.backend.data.entities.User;
import com.gs.meetingbuddy.backend.repositories.MeetingRepository;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MeetingService implements FilterableCrudService<Meeting> {

	private final MeetingRepository meetingRepository;

	@Autowired
	public MeetingService(MeetingRepository meetingRepository) {
		this.meetingRepository = meetingRepository;
	}

	public Meeting findById(long id) {
		return meetingRepository.findById(id).get();
	}

	@Override
	//Data provider calls this function if UI search filter is activated -  from UI it is called as  dataProvider.setFilter(event.getValue()) ; 
	//Must match with a function countAnyMatching(...)  !
	public Page<Meeting> findAnyMatching(Optional<String> filter, Pageable pageable) {
		if (filter.isPresent()) {
			String repositoryFilter = "%" + filter.get() + "%";
			return meetingRepository.findByDescriptionLikeIgnoreCaseOrLocationLikeIgnoreCase(repositoryFilter,
					repositoryFilter, pageable);// findByDescriptionLikeIgnoreCase
		} else {
			return find(pageable);
		}
	}

	@Override
	//Must match with a function findAnyMatching(...)  !
	public long countAnyMatching(Optional<String> filter) {
		if (filter.isPresent()) {
			String repositoryFilter = "%" + filter.get() + "%";
			return meetingRepository.countByDescriptionLikeIgnoreCaseOrLocationLikeIgnoreCase(repositoryFilter,
					repositoryFilter); // countByDescriptionLikeIgnoreCase
		} else {
			return count();
		}
	}

	public Page<Meeting> find(Pageable pageable) {
		return meetingRepository.findBy(pageable);
	}

	@Override
	public JpaRepository<Meeting, Long> getRepository() {
		return meetingRepository;
	}

	@Override
	public Meeting createNew(User currentUser) {
		return new Meeting();
	}

	@Override
	@Transactional
	public void delete(User currentUser, Meeting entity) {
		FilterableCrudService.super.delete(currentUser, entity);
	}

	// @Transactional - use when you will also include other save/delete services
	// (e.g. meeting , user, attend and conclusion - if edited together)
	@Override
	public Meeting save(User currentUser, Meeting entity) { // also validate currentUser if it has rights to do so -  todo
		try {
			return FilterableCrudService.super.save(currentUser, entity);
		} catch (DataIntegrityViolationException e) {
			throw new UserFriendlyDataException(
					"There is already a meeting with that name. Please select a unique name for the meeting.");
		}

	}

}