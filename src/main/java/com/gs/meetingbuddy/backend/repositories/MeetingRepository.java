package com.gs.meetingbuddy.backend.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import com.gs.meetingbuddy.backend.data.entities.*;

public interface MeetingRepository extends JpaRepository<Meeting, Long> {
	
	Page<Meeting> findBy(Pageable page);

	Page<Meeting> findByDescriptionLikeIgnoreCase(String name, Pageable page);
	int countByDescriptionLikeIgnoreCase(String name);

	Page<Meeting> findByDescriptionLikeIgnoreCaseOrLocationLikeIgnoreCase(String repositoryFilter,String repositoryFilter2, Pageable pageable);
	int countByDescriptionLikeIgnoreCaseOrLocationLikeIgnoreCase(String repositoryFilter,String repositoryFilter2);
	
	//TODO 
	//list of all users per meeting = list of attendees 
}
