package com.gs.meetingbuddy.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

import com.gs.meetingbuddy.backend.data.entities.*;

public interface AttendRepository extends JpaRepository<Attend,Long> {
 
	Set<Attend> findByMeetingId(Long meetingId);
	
	//TODO 
	//list of all users , 
	//list of meetings 
}
