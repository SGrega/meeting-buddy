package com.gs.meetingbuddy.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gs.meetingbuddy.backend.data.entities.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserRepository extends JpaRepository<User, Long> {
 
	User findByEmailIgnoreCase(String email);

	Page<User> findBy(Pageable pageable);

	long countByEmailLikeIgnoreCaseOrFullNameLikeIgnoreCaseOrPhoneLikeIgnoreCase(String emailLike, String fullNameLike,
			String phone);

	Page<User> findByEmailLikeIgnoreCaseOrFullNameLikeIgnoreCaseOrPhoneLikeIgnoreCase(String email, String name,
			String phone, Pageable pageable);

	// TODO
	// list of user meetings - mark as attendee
}
