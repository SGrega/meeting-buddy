package com.gs.meetingbuddy.ui.views.user;

import com.vaadin.flow.component.grid.Grid;
import com.gs.meetingbuddy.backend.data.entities.User;

/**
 * Grid of users, handling the visual presentation and filtering of a set of
 * items.
 */
public class UserGrid extends Grid<User> {

	public UserGrid() {
		super();
		setSizeFull();

		addColumn(User::getFullName).setHeader("Full name");
		addColumn(User::getEmail).setHeader("Email");
		addColumn(User::getPhone).setHeader("Phone numer");
		addColumn(User::getType).setHeader("Type");
		//or simply: setColumns("fullName", "email", "phone", "type");

		setMinHeight("300px");
		//sort("Name", SortDirection.ASCENDING); OR via data provider
	}

	public User getSelectedRow() {
		return asSingleSelect().getValue();
	}

	public void refresh(User user) {
		getDataCommunicator().refresh(user);
	}

}
