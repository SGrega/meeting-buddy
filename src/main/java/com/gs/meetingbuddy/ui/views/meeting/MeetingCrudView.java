package com.gs.meetingbuddy.ui.views.meeting;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.QuerySortOrderBuilder;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

import com.gs.meetingbuddy.backend.data.entities.Meeting;
import com.gs.meetingbuddy.backend.repositories.MeetingRepository;
import com.gs.meetingbuddy.backend.services.MeetingService;
import com.gs.meetingbuddy.ui.utils.dataproviders.CrudEntityDataProvider;
import com.gs.meetingbuddy.ui.views.*;

/**
 * A view for performing create-read-update-delete operations on meetings.
 *
 * See also {@link MeetingCrudController} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
//@Secured(Role.ADMIN) - future user access control
@Route(value = "Meeting", layout = MainLayout.class)
@RouteAlias(value = "", layout = MainLayout.class) //"" representing root/index View 
public class MeetingCrudView extends HorizontalLayout
        implements HasUrlParameter<String> {

    public static final String VIEW_NAME = "Meeting";

    //ui related
    private MeetingGrid grid;
    private MeetingForm form;
    private TextField filter;
    private Button newMeetingBtn;
    
    //data related
    private MeetingRepository mRepository;
    CrudEntityDataProvider<Meeting> dataProvider ;
    private MeetingService meetingService;
    
    private MeetingCrudController viewController ; 

    
    public MeetingCrudView(MeetingRepository mRepository) {
    	this.mRepository=mRepository;
    	meetingService= new MeetingService(mRepository);
    	dataProvider = new CrudEntityDataProvider<>(meetingService);
    	
     	dataProvider.setSortOrders(new QuerySortOrderBuilder().thenAsc("startDtime").build());
    	 viewController = new MeetingCrudController(this,meetingService); 
    	 
        setSizeFull();
        HorizontalLayout topLayout = createTopBar();

        
       
        grid = new MeetingGrid();
        grid.setDataProvider(dataProvider);
        grid.asSingleSelect().addValueChangeListener(
                event -> viewController.rowSelected(event.getValue()));
 
        form = new MeetingForm(viewController);
   

        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.add(topLayout);
        barAndGridLayout.add(grid);
        barAndGridLayout.setFlexGrow(1, grid);
        barAndGridLayout.setFlexGrow(0, topLayout);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.expand(grid);

        add(barAndGridLayout);
        add(form);

        viewController.init();
    }

    public HorizontalLayout createTopBar() {
    	// Apply the filter to grid's data provider. TextField value is never null
        filter = new TextField();
        filter.setPlaceholder("Filter description or location");
        
        // Activate on every key change/press - if removed, Enter Key press would be needed
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(event ->  dataProvider.setFilter(event.getValue()) );
        filter.addFocusShortcut(Key.KEY_F, KeyModifier.CONTROL);

        newMeetingBtn = new Button("New meeting");
        newMeetingBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        newMeetingBtn.setIcon(VaadinIcon.PLUS_CIRCLE.create());
        newMeetingBtn.addClickListener(click -> viewController.newMeeting());
        // CTRL+N will create a new window which is unavoidable
        newMeetingBtn.addClickShortcut(Key.KEY_N, KeyModifier.ALT);

        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.setWidth("100%");
        topLayout.add(filter);
        topLayout.add(newMeetingBtn);
        topLayout.setVerticalComponentAlignment(Alignment.START, filter);
        topLayout.expand(filter);
        return topLayout;
    }

    // UI related functions
 	//
    
    public void showError(String msg) {
    	showMyInfoNotification(msg);
    }

    public void showSaveNotification(String msg) {
    	showMyInfoNotification(msg);
    }

    public void setNewMeetingEnabled(boolean enabled) {
        newMeetingBtn.setEnabled(enabled);
    }

    public void clearSelection() {
        grid.getSelectionModel().deselectAll();
    }

    public void selectRow(Meeting row) {
        grid.getSelectionModel().select(row);
    }

    public Meeting getSelectedRow() {
        return grid.getSelectedRow();
    }

    public void updateMeeting(Meeting meeting) {
        grid.getDataProvider().refreshAll();//refreshItem(meeting); --because of re-ordering , should be full!  
    }

    public void removeMeeting(Meeting meeting) {
        grid.getDataProvider().refreshAll();
    }

    public void editMeeting(Meeting meeting) {
        showForm(meeting != null);
        form.editMeeting(meeting);
    }

    public void showForm(boolean show) {
        form.setVisible(show);

        /* FIXME The following line should be uncommented when the CheckboxGroup
         * issue is resolved. The category CheckboxGroup throws an
         * IllegalArgumentException when the form is disabled.
         */
        //form.setEnabled(show);
    }
    
    public void showMyInfoNotification(String str) {
    	Div content = new Div();
    	content.addClassName("myInfoNotification");
    	content.setText(str);

    	Notification notification = new Notification( content  );
    	notification.setDuration(4000);
    	notification.open();
    	
    }

    // Navigation parameters, let a View Controller to handle it
    @Override
    public void setParameter(BeforeEvent event,
                             @OptionalParameter String parameter) {
        viewController.enter(parameter);
    }
}
