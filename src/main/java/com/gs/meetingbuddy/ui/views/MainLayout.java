package com.gs.meetingbuddy.ui.views;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

import com.gs.meetingbuddy.ui.views.meeting.MeetingCrudView;
import com.gs.meetingbuddy.ui.views.user.UserCrudView;
import com.gs.meetingbuddy.ui.utils.authentication.AccessControlFactory;

/**
 * The main layout. Contains the navigation menu.
 */
@HtmlImport("css/shared-styles.html")
@Theme(value = Lumo.class)
@PWA(name = "Meeting Buddy", shortName = "MeetingBuddy")
public class MainLayout extends FlexLayout implements RouterLayout {
	private Menu menu;

	public MainLayout() {
		setSizeFull();
		setClassName("main-layout");

		menu = new Menu();

		menu.addView(UserCrudView.class, UserCrudView.VIEW_NAME, VaadinIcon.USER.create());
		menu.addView(MeetingCrudView.class, MeetingCrudView.VIEW_NAME, VaadinIcon.EDIT.create());
		menu.addView(AboutView.class, AboutView.VIEW_NAME, VaadinIcon.INFO_CIRCLE.create());

		add(menu);
	}

	@Override
	protected void onAttach(AttachEvent attachEvent) {
		super.onAttach(attachEvent);

		attachEvent.getUI().addShortcutListener(
				() -> AccessControlFactory.getInstance().createAccessControl().signOut(), Key.KEY_L,
				KeyModifier.CONTROL);

	}
}
