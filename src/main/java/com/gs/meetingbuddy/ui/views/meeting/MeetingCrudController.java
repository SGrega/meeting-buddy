package com.gs.meetingbuddy.ui.views.meeting;

import com.vaadin.flow.component.UI;
import com.gs.meetingbuddy.ui.utils.authentication.*;
import com.gs.meetingbuddy.backend.data.entities.*;
import com.gs.meetingbuddy.backend.services.MeetingService;

import java.io.Serializable;

/**
 * This class provides an interface for the logical operations between the CRUD
 * view, its parts like the meeting editor form and the data source, including
 * fetching and saving meetings.
 *
 * Having this separate from the view makes it easier to test various parts of
 * the system separately, and to e.g. provide alternative views for the same
 * data.
 */
public class MeetingCrudController implements Serializable {

	private MeetingCrudView view;

	private MeetingService meetingService;

	public MeetingCrudController(MeetingCrudView simpleCrudView, MeetingService meetingService) {
		view = simpleCrudView;
		this.meetingService = meetingService;
	}

	public void init() {
		editMeeting(null);
		// Hide and disable if not admin
		if (!AccessControlFactory.getInstance().createAccessControl().isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
			view.setNewMeetingEnabled(false);
		}
	}

	/**
	 * Update the fragment without causing navigator to change view
	 */
	private void setFragmentParameter(String meetingId) {
		String fragmentParameter;
		if (meetingId == null || meetingId.isEmpty()) {
			fragmentParameter = "";
		} else {
			fragmentParameter = meetingId;
		}

		UI.getCurrent().navigate(MeetingCrudView.class, fragmentParameter);
	}

	/**
	 * Actions based on URL navigation - e.g. /Meeting/new or /Meeting/1
	 */
	public void enter(String meetingId) {
		if (AccessControlFactory.getInstance().createAccessControl().isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
			if (meetingId != null && !meetingId.isEmpty()) {
				if (meetingId.equals("new")) {
					newMeeting();
				} else {
					// Ensure this is selected even if coming directly here from
					// login
					try {
						int pid = Integer.parseInt(meetingId);
						Meeting meeting = findMeeting(pid);
						view.selectRow(meeting);
					} catch (Exception e) {// NumberFormat
						setFragmentParameter("");
						view.showMyInfoNotification("Meeting with provided value does not exist!");
					}
				}
			} else {
				view.showForm(false);
			}
		}
	}

	private Meeting findMeeting(long meetingId) {
		return meetingService.findById(meetingId);
	}

	public void saveMeeting(Meeting meeting) {
		boolean newMeeting = meeting.getId() == -1; // isNewMeeting();
		// action on DB
		meetingService.save(null, meeting);
		// action on UI
		view.clearSelection();
		view.updateMeeting(meeting);
		setFragmentParameter("");
		view.showSaveNotification("\"" + meeting.getDescription() + "\"" // MeetingName()
				+ (newMeeting ? " created" : " updated"));

	}

	public void deleteMeeting(Meeting meeting) {
		if (AccessControlFactory.getInstance().createAccessControl().isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
			// action on DB
			meetingService.delete(null, meeting);
			// action on UI
			view.clearSelection();
			view.removeMeeting(meeting);
			setFragmentParameter("");
			view.showSaveNotification("\"" + meeting.getDescription() + "\" removed");
		}
	}

	public void editMeeting(Meeting meeting) {
		if (AccessControlFactory.getInstance().createAccessControl().isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
			if (meeting == null || meeting.getId() == -1) {
				setFragmentParameter("");
			} else {
				setFragmentParameter(meeting.getId() + "");
			}
			view.editMeeting(meeting);
		}
	}

	public void newMeeting() {
		if (AccessControlFactory.getInstance().createAccessControl().isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
			view.clearSelection();
			setFragmentParameter("new");
			view.editMeeting(new Meeting());
		}
	}

	public void rowSelected(Meeting meeting) {
		if (AccessControlFactory.getInstance().createAccessControl().isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
			editMeeting(meeting);
		}
	}

	public void cancelMeeting() {
		setFragmentParameter("");
		view.clearSelection();
	}

}
