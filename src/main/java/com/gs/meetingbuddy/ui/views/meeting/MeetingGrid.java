package com.gs.meetingbuddy.ui.views.meeting;

import com.vaadin.flow.component.grid.Grid;
import com.gs.meetingbuddy.backend.data.entities.Attend;
import com.gs.meetingbuddy.backend.data.entities.Meeting;

/**
 * Grid of meetings, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class MeetingGrid extends Grid<Meeting> {

	public MeetingGrid() {
		super();
		setSizeFull();

		addColumn(Meeting::getDescription)
			.setHeader("Description ")
			.setFlexGrow(20);// .setWidth("250px");

		addColumn(Meeting::getLocation)
			.setHeader("Location").
			setFlexGrow(15);// .setWidth("200px");

		addColumn(Meeting::getStartDtime)
			.setHeader("StartTime")
			.setKey("startDtime")
			.setFlexGrow(15)
			.setSortable(true);// .setWidth("175px");

		addColumn(Meeting::getEndDtime)
			.setHeader("EndTime")
			.setKey("endDtime")
			.setFlexGrow(15)
			.setSortable(true);// .setWidth("175px");

		//Demo: only to show some extra info related to a meeting e.g. users that are entered as attendees of specific meeting AND conclusions (empty for now)
		addColumn(meeting -> {	return meeting.getAttendeesString();  })
			.setHeader("Attendees")
			.setFlexGrow(31);

		addColumn(conclusion -> { return "";})
			.setHeader("Conclusions")
			.setFlexGrow(3);// .setWidth("100px"); 

		setMinHeight("300px");

	}

	public Meeting getSelectedRow() {
		return asSingleSelect().getValue();
	}

	public void refresh(Meeting meeting) {
		getDataCommunicator().refresh(meeting);
	}

	// Demo listing of users that are entered as attendees of specific meeting -- meeting role + user name + if going status
	public String getAttendeesString(Meeting meeting) {

		if (meeting.getAttendees().size() != 0) {
			String msg = "";
			for (Attend attObj : meeting.getAttendees()) {
				msg += "[(" + attObj.getRole() + ") " + attObj.getUserObject().getFullName() + " :" + attObj.getGoing()+ "] ";
			}
			return msg;
		} else {
			return "";
		}
	}

}
