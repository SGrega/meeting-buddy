package com.gs.meetingbuddy.ui.views;

import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.Version;
import com.gs.meetingbuddy.ui.views.MainLayout;

@Route(value = "About", layout = MainLayout.class)
@PageTitle("About")
public class AboutView extends HorizontalLayout {

    public static final String VIEW_NAME = "About";

    public AboutView() {
        add(VaadinIcon.INFO_CIRCLE.create());
        add(new Span("This is a simple CRUD app built with Spring Boot + Vaadin Flow. " +
				        "  It uses Spring Data JPA and MySQL database by default (can also use H2 or any other - DB independant). " +
				        "  See the GitHub/Bitbucket project page for more details. "+
				        "  App version: 0.1-((pre-)pre-)alpha. By G.S. ;)"));
      
        setSizeFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
        setAlignItems(Alignment.CENTER);
    }
}
