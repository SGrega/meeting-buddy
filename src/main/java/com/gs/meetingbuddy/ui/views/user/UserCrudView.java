package com.gs.meetingbuddy.ui.views.user;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.QuerySortOrderBuilder;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import com.gs.meetingbuddy.backend.data.entities.User;
import com.gs.meetingbuddy.backend.repositories.UserRepository;
import com.gs.meetingbuddy.backend.services.UserService;
import com.gs.meetingbuddy.ui.utils.dataproviders.CrudEntityDataProvider;
import com.gs.meetingbuddy.ui.views.*;

/**
 * A view for performing create-read-update-delete operations on users.
 *
 * See also {@link UserCrudController} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
//@Secured(Role.ADMIN) - future user access control
@Route(value = "User", layout = MainLayout.class)
public class UserCrudView extends HorizontalLayout implements HasUrlParameter<String> {

	public static final String VIEW_NAME = "User";

	// ui related
	private UserGrid grid;
	private UserForm form;
	private TextField filter;
	private Button newUserBtn;

	// data related
	private UserRepository mRepository;
	private CrudEntityDataProvider<User> dataProvider;
	private UserService userService;

	private UserCrudController viewController;

	public UserCrudView(UserRepository mRepository) {
		this.mRepository = mRepository;

		userService = new UserService(mRepository);
		dataProvider = new CrudEntityDataProvider<>(userService);
		dataProvider.setSortOrders(new QuerySortOrderBuilder().thenAsc("fullName").build());
		viewController = new UserCrudController(this, userService);

		setSizeFull();
		HorizontalLayout topLayout = createTopBar();

		grid = new UserGrid();
		grid.setDataProvider(dataProvider);
		grid.asSingleSelect().addValueChangeListener(event -> viewController.rowSelected(event.getValue()));

		form = new UserForm(viewController);

		VerticalLayout barAndGridLayout = new VerticalLayout();
		barAndGridLayout.add(topLayout);
		barAndGridLayout.add(grid);
		barAndGridLayout.setFlexGrow(1, grid);
		barAndGridLayout.setFlexGrow(0, topLayout);
		barAndGridLayout.setSizeFull();
		barAndGridLayout.expand(grid);

		add(barAndGridLayout);
		add(form);

		viewController.init();
	}

	public HorizontalLayout createTopBar() {
		// Apply the filter to grid's data provider. TextField value is never null
		filter = new TextField();
		filter.setPlaceholder("Filter full name or email or phone");

		// Activate on every key change/press - if removed, Enter Key press would be needed
		filter.setValueChangeMode(ValueChangeMode.EAGER);
		// Replace listing with filtered content when user changes filter
		filter.addValueChangeListener(event -> dataProvider.setFilter(event.getValue()));
		filter.addFocusShortcut(Key.KEY_F, KeyModifier.CONTROL);

		newUserBtn = new Button("New user");
		newUserBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		newUserBtn.setIcon(VaadinIcon.PLUS_CIRCLE.create());
		newUserBtn.addClickListener(click -> viewController.newUser());
		// CTRL+N will create a new window which is unavoidable
		newUserBtn.addClickShortcut(Key.KEY_N, KeyModifier.ALT);

		HorizontalLayout topLayout = new HorizontalLayout();
		topLayout.setWidth("100%");
		topLayout.add(filter);
		topLayout.add(newUserBtn);
		topLayout.setVerticalComponentAlignment(Alignment.START, filter);
		topLayout.expand(filter);
		return topLayout;
	}

	// UI related functions
	//
	public void showError(String msg) {
		showMyInfoNotification(msg);
	}

	public void showSaveNotification(String msg) {
		showMyInfoNotification(msg);
	}

	public void setNewUserEnabled(boolean enabled) {
		newUserBtn.setEnabled(enabled);
	}

	public void clearSelection() {
		grid.getSelectionModel().deselectAll();
	}

	public void selectRow(User row) {
		grid.getSelectionModel().select(row);
	}

	public User getSelectedRow() {
		return grid.getSelectedRow();
	}

	public void updateUser(User user) {
		grid.getDataProvider().refreshAll();// refreshItem(user); --because of ordering , should be full!
	}

	public void removeUser(User user) {
		grid.getDataProvider().refreshAll();
	}

	public void editUser(User user) {
		showForm(user != null);
		form.editUser(user);
	}

	public void showForm(boolean show) {
		form.setVisible(show);

		/*
		 * FIXME The following line should be uncommented when the CheckboxGroup issue
		 * is resolved. The category CheckboxGroup throws an IllegalArgumentException
		 * when the form is disabled.
		 */
		// form.setEnabled(show);
	}

	public void showMyInfoNotification(String str) {
		Div content = new Div();
		content.addClassName("myInfoNotification");
		content.setText(str);

		Notification notification = new Notification(content);
		notification.setDuration(4000);
		notification.open();

	}

	// Navigation parameters, let a View Controller to handle it
	@Override
	public void setParameter(BeforeEvent event, @OptionalParameter String parameter) {
		viewController.enter(parameter);
	}
}
