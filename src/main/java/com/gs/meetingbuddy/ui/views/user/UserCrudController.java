package com.gs.meetingbuddy.ui.views.user;

import com.vaadin.flow.component.UI;
import com.gs.meetingbuddy.ui.utils.authentication.*;
import com.gs.meetingbuddy.backend.data.entities.*;
import com.gs.meetingbuddy.backend.services.UserService;

import java.io.Serializable;

/**
 * This class provides an interface for the logical operations between the CRUD
 * view, its parts like the user editor form and the data source, including
 * fetching and saving users.
 *
 * Having this separate from the view makes it easier to test various parts of
 * the system separately, and to e.g. provide alternative views for the same
 * data.
 */
public class UserCrudController implements Serializable {

	private UserCrudView view;

	private UserService userService;
	private User selectedUser;

	public UserCrudController(UserCrudView simpleCrudView, UserService userService) {  
		view = simpleCrudView;
		this.userService = userService;
	}

	public void init() {
		editUser(null);
		// Hide and disable if not admin
		if (!AccessControlFactory.getInstance().createAccessControl().isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
			view.setNewUserEnabled(false);
		}
	}

	/**
	 * Update the fragment without causing navigator to change view
	 */
	private void setFragmentParameter(String userId) {
		String fragmentParameter;
		if (userId == null || userId.isEmpty()) {
			fragmentParameter = "";
		} else {
			fragmentParameter = userId;
		}

		UI.getCurrent().navigate(UserCrudView.class, fragmentParameter);
	}

	/**
	 * Actions based on URL navigation parameters - e.g. /User/new or /User/1
	 */
	public void enter(String userId) {
		if (AccessControlFactory.getInstance().createAccessControl().isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
			if (userId != null && !userId.isEmpty()) {
				if (userId.equals("new")) {
					newUser();
				} else {
					// Ensure this is selected even if coming directly here from
					// login
					try {
						int pid = Integer.parseInt(userId);
						User user = findUser(pid);
						view.selectRow(user);
					} catch (Exception e) { // or use specific NumberFormat
						setFragmentParameter("");
						view.showMyInfoNotification("User with provided value does not exist!");
					}
				}
			} else {
				view.showForm(false);
			}
		}
	}

	private User findUser(long userId) {
		return userService.findById(userId);
	}

	public void saveUser(User user) {
		boolean newUser = user.getId() == -1; // isNewUser();
		view.clearSelection();
		// action on DB
		userService.save(null, user);
		// action on UI
		view.updateUser(user);
		setFragmentParameter("");
		view.showSaveNotification("\"" + user.getFullName() + "\"" 
				+ (newUser ? " created" : " updated"));

	}

	public void deleteUser(User user) {
		if (AccessControlFactory.getInstance().createAccessControl().isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
			// action on DB
			userService.delete(null, user);
			// action on UI
			view.clearSelection();
			view.removeUser(user);
			setFragmentParameter("");
			view.showSaveNotification("\"" + user.getFullName() + "\" removed");
		}
	}

	public void editUser(User user) {
		if (AccessControlFactory.getInstance().createAccessControl().isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
			if (user == null || user.getId() == -1) {
				setFragmentParameter("");
			} else {
				setFragmentParameter(user.getId() + "");
			}
			view.editUser(user);
		}
	}

	public void newUser() {
		if (AccessControlFactory.getInstance().createAccessControl().isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
			view.clearSelection();
			setFragmentParameter("new");
			view.editUser(new User());
		}
	}

	public void rowSelected(User user) {
		if (AccessControlFactory.getInstance().createAccessControl().isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
			editUser(user);
		}
	}

	public void cancelUser() {
		setFragmentParameter("");
		view.clearSelection();
	}

}
