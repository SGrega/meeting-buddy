package com.gs.meetingbuddy.ui.views.meeting;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.value.ValueChangeMode;

import com.gs.meetingbuddy.backend.data.entities.*;// Meeting;

/**
 * A form for editing a single meeting.
 */
public class MeetingForm extends Div {

	// data related
	private MeetingCrudController viewController;
	private Binder<Meeting> binder;
	private Meeting currentMeeting;

	// ui related
	private VerticalLayout content;

	private TextField description = new TextField();
	private TextField location = new TextField();;
	private TextField startDtime = new TextField();;
	private TextField endDtime = new TextField();;

	private Button save;
	private Button discard;
	private Button cancel;
	private Button delete;

	
	public MeetingForm(MeetingCrudController meetingCrudLogic) {
		super();
		setClassName("general-form");

		content = new VerticalLayout();
		content.setSizeUndefined();
		add(content);

		viewController = meetingCrudLogic;

		description = new TextField("Meeting description");
		description.setWidth("100%");
		description.setRequired(true);
		description.setValueChangeMode(ValueChangeMode.EAGER);
		content.add(description);

		location = new TextField("Meeting location");
		location.setWidth("100%");
		location.setRequired(true);
		location.setValueChangeMode(ValueChangeMode.EAGER);
		content.add(location);

		startDtime = new TextField("startDtime");
		startDtime.setWidth("100%");
		startDtime.setRequired(true);
		startDtime.setValueChangeMode(ValueChangeMode.EAGER);
		content.add(startDtime);

		endDtime = new TextField("endDtime");
		endDtime.setWidth("100%");
		endDtime.setRequired(true);
		endDtime.setValueChangeMode(ValueChangeMode.EAGER);
		content.add(endDtime);

		// Bind Meeting class variables with UI components
		// TODO: improve UI field validators!
		binder = new BeanValidationBinder<>(Meeting.class);

		//binding items/fields and  shorthand for requiring the field to be non-empty(todo:validation)
		binder.forField(description)
			.asRequired("Every meeting must have a description")
			.bind(Meeting::getDescription, Meeting::setDescription);

		binder.forField(location)
			.asRequired("Required field")
			.bind(Meeting::getLocation, Meeting::setLocation);

		 
		// Special custom binding from Date to String and back to Date - With lambda expressions
		binder.forField(startDtime).asRequired("Required field").bind(dat -> (dat.getStartDtime() == null ? ""
				: new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dat.getStartDtime())), (dat, val) -> {
					try {
						dat.setStartDtime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(val));
					} catch (ParseException ex) {
						ex.printStackTrace();
						Notification.show(ex.getMessage());
					}
				});

		binder.forField(endDtime).asRequired("Required field").bind(dat2 -> dat2.getEndDtime() == null ? ""
				: new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dat2.getEndDtime()), (dat2, val2) -> {
					try {
						dat2.setEndDtime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(val2));
					} catch (ParseException ex) {
						ex.printStackTrace();
						Notification.show(ex.getMessage());
					}
				});

		// Configure and style components
		// setSpacing(true);

		// bind using naming conventions
		binder.bindInstanceFields(this);

		
		//// Create and configure general form components e.g. save, delete,cancel, discard
		// 

		// enable/disable save button while editing
		binder.addStatusChangeListener(event -> {
			boolean isValid = !event.hasValidationErrors();
			boolean hasChanges = binder.hasChanges();
			save.setEnabled(hasChanges && isValid);
			discard.setEnabled(hasChanges);
		});

		save = new Button("Save");
		save.setWidth("100%");
		save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		save.addClickListener(event -> {
			if (currentMeeting != null && binder.writeBeanIfValid(currentMeeting)) {
				viewController.saveMeeting(currentMeeting);
			}
		});
		save.addClickShortcut(Key.KEY_S, KeyModifier.CONTROL);

		discard = new Button("Discard changes");
		discard.setWidth("100%");
		discard.addClickListener(event -> viewController.editMeeting(currentMeeting));

		cancel = new Button("Cancel");
		cancel.setWidth("100%");
		cancel.addClickListener(event -> viewController.cancelMeeting());
		cancel.addClickShortcut(Key.ESCAPE);
		getElement().addEventListener("keydown", event -> viewController.cancelMeeting())
				.setFilter("event.key == 'Escape'");

		delete = new Button("Delete");
		delete.setWidth("100%");
		delete.addThemeVariants(ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_PRIMARY);
		delete.addClickListener(event -> {
			if (currentMeeting != null) {
				viewController.deleteMeeting(currentMeeting);
			}
		});

		content.add(save, discard, delete, cancel);
	}

	public void editMeeting(Meeting meeting) {
		if (meeting == null) {
			meeting = new Meeting();
		}
		delete.setVisible(!(meeting.getId() == -1)); // isNewMeeting());
		currentMeeting = meeting;
		binder.readBean(meeting);
	}
}
