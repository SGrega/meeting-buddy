package com.gs.meetingbuddy.ui.views.user;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.gs.meetingbuddy.backend.data.UserTypeENUM;
import com.gs.meetingbuddy.backend.data.entities.*;

/**
 * A form for editing a single user.
 */
public class UserForm extends Div {

	private VerticalLayout content;

	private TextField fullName = new TextField();
	private TextField password = new TextField();
	private TextField email = new TextField();;
	private TextField phone = new TextField();;

	private Button save;
	private Button discard;
	private Button cancel;
	private Button delete;

	private UserCrudController viewController;
	private Binder<User> binder;
	private User currentUser;

	public UserForm(UserCrudController userCrudLogic) {
		super();
		setClassName("general-form");

		content = new VerticalLayout();
		content.setSizeUndefined();
		add(content);

		viewController = userCrudLogic;

		fullName = new TextField("Full name");
		fullName.setWidth("100%");
		fullName.setRequired(true);
		fullName.setValueChangeMode(ValueChangeMode.EAGER);
		content.add(fullName);

		password = new TextField("Password");
		password.setWidth("100%");
		password.setRequired(true);
		password.setValueChangeMode(ValueChangeMode.EAGER);
		content.add(password);

		email = new TextField("Email");
		email.setWidth("100%");
		email.setRequired(true);
		email.setValueChangeMode(ValueChangeMode.EAGER);
		content.add(email);

		phone = new TextField("Phone");
		phone.setWidth("100%");
		phone.setRequired(true);
		phone.setValueChangeMode(ValueChangeMode.EAGER);
		content.add(phone);

		// Get list of strings from enum type
		List<String> enumNames = Stream.of(UserTypeENUM.values()).map(Enum::name).collect(Collectors.toList());

		// Create select UI component
		Select<String> type = new Select<>();
		type.setItems(enumNames);
		type.setPlaceholder("- select user type -");
		type.setLabel("Type");
		content.add(type);

		// Bind User class variables with UI components
		// TODO: improve UI field validators!
		binder = new BeanValidationBinder<>(User.class);

		//binding items/fields and  shorthand for requiring the field to be non-empty(todo:validation)
		binder.forField(fullName)
				.asRequired("required field").bind(User::getFullName, User::setFullName);

		binder.forField(email)
				.asRequired("required field").bind(User::getEmail, User::setEmail);

		binder.forField(phone)
				.asRequired("required field").bind(User::getPhone, User::setPhone);

		binder.forField(password)
				.asRequired("required field").bind(User::getPassword, User::setPassword);

		// User variable named  *type* (UserTypeENUM) is Enum Java type, so we should make custom
		// binding - With lambda expressions
		binder.forField(type).asRequired("required field")
				.bind(obj -> (obj.getType() == null ? "" : obj.getType().toString()), (obj, val) -> {
					try {
						obj.setType(UserTypeENUM.valueOf(val));
					} catch (Exception ex) {
						ex.printStackTrace();
						Notification.show(ex.getMessage());
					}
				});
		
		// bind using naming conventions
		binder.bindInstanceFields(this);

		
		//// Create and configure general form components e.g. save, delete,cancel, discard
		// 

		// enable/disable save button while editing
		binder.addStatusChangeListener(event -> {
			boolean isValid = !event.hasValidationErrors();
			boolean hasChanges = binder.hasChanges();
			save.setEnabled(hasChanges && isValid);
			discard.setEnabled(hasChanges);
		});

		save = new Button("Save");
		save.setWidth("100%");
		save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		save.addClickListener(event -> {
			if (currentUser != null && binder.writeBeanIfValid(currentUser)) {
				viewController.saveUser(currentUser);
			}
		});
		save.addClickShortcut(Key.KEY_S, KeyModifier.CONTROL);

		discard = new Button("Discard changes");
		discard.setWidth("100%");
		discard.addClickListener(event -> viewController.editUser(currentUser));

		cancel = new Button("Cancel");
		cancel.setWidth("100%");
		cancel.addClickListener(event -> viewController.cancelUser());
		cancel.addClickShortcut(Key.ESCAPE);
		getElement().addEventListener("keydown", event -> viewController.cancelUser())
				.setFilter("event.key == 'Escape'");

		delete = new Button("Delete");
		delete.setWidth("100%");
		delete.addThemeVariants(ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_PRIMARY);
		delete.addClickListener(event -> {
			if (currentUser != null) {
				viewController.deleteUser(currentUser);
			}
		});

		content.add(save, discard, delete, cancel);
	}

	public void editUser(User user) {
		if (user == null) {
			user = new User();
		}
		delete.setVisible(!(user.getId() == -1)); // isNewUser());
		currentUser = user;
		binder.readBean(user);
	}
}
