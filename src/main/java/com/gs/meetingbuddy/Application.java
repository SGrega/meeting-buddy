package com.gs.meetingbuddy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.gs.meetingbuddy.backend.repositories.*;
import com.gs.meetingbuddy.backend.data.entities.*;
import com.gs.meetingbuddy.ui.views.*;

//@Configuration
//@ComponentScan({"com.gs.meetingbuddy.backend.data.entities", "com.gs.meetingbuddy.backend.data.repository"})
@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class);
	}

	@Bean
	public CommandLineRunner loadData() {
		return (args) -> {
			
		};
	}

}
