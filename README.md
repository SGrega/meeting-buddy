
#About
Experimental meeting planner aka Meeting Buddy. In early (pre-(pre-))alpha version :S
 

Using Spring Data JPA CRUD with Vaadin Flow  

A simple CRUD example with [Spring Data JPA](http://projects.spring.io/spring-data-jpa/) and [Vaadin](https://vaadin.com). Uses [Spring Boot](http://projects.spring.io/spring-boot/) for easy project setup and development. Helps you to get started with basic JPA backed applications and [Vaadin Spring Boot](https://vaadin.com/addon/vaadin-spring-boot) integration library.



##How to play with this example
note: do not forget to update file with your data source \src\main\resources\application.properties

###Suggested method

* Clone the project
* Import to your favorite IDE
* Execute the main method from Application class

###Just execute it with Maven

```
git clone https://---find--.git
cd meeting-buddy
mvn spring-boot:run
```

###Just create the package and run it

The built .jar file is auto-runnable, so as you can move it to any computer having java installed and run the app. 

```
git clone https://---find--.git
cd meeting-buddy
mvn package
java -jar target/meeting-buddy-0.0.1-SNAPSHOT.jar
```



##Documentation 

### Some files inside */doc/* folder includes:
* DB Schema diagram 
* Full DB export - DDLs and Test Data (doc\db\MySQL-DB-export(DDL+data).sql)
* App organisation diagram 
* UI Screenshots
* ...

###Dependencies
* Vaadin Flow (version 13.0.3)
* Spring-framework Boot (version 2.1.3.RELEASE, primarily to use Spring Data API)
* MySQL (in our example case -  designed to be compatible to any other DB provider )


###Data flow (both directions):
entity <-> factory <-> service  <-> data provider  <-> controller -> view

Backend

* entity - mapping db table to a Java class
* factory - db queries (using Spring-framework Data API to dynamically create queries)
* service - secure layer between DB and UI requests and including some backend logic

Frontend/UI

* data provider - basically a frontend service that connects to backend service. Vaadin Flow Grid need it in such way. UI can also use backend service directly.
* controller - containing view's main logic
* view - graphical components

###Done
* Full UI layout and navigation
* Demo security - login and user type validation (admin: all CRUD options, demo:read only)
* CRUD for User pages (grid and form components - more or less completed)
* CRUD for Meeting pages (grid and form components - grid fine, but form does not includes all UI components just yet, e.g. editing list of attendees)
* Basic error handling and notifications
* Grid filtering
* ...


###TODO:
Many things, including to complete basic functionalities (e.g. to connect login form with DB users,  Conclusion pages, Meeting pages,...)


###PICTURES

####DB schema (simplified  diagram)

![SchemaDiagram](doc/db/schema-simlified-diagram.png)


See full DB dump (DDLs and data) on doc/db/

####Screenshots of UI

See folder doc/ui-screenshots/*  for all other images


LogiIn form
![Login](doc/ui-screenshots/LogIn.png)

User editing
![useredit](doc/ui-screenshots/user_fullscreen_edit.png)

Meeting editing
![meetingedit](doc/ui-screenshots/meeting_fullscreen_edit_item7.png)

Grid filtering
![filter](doc/ui-screenshots/user_fullscreen_filter.png)
